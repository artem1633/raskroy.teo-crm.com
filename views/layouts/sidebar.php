<?php 
use yii\helpers\Url;
use yii\helpers\Html;
use app\widgets\Menu;

$pathInfo = Yii::$app->request->pathInfo;
?>

<div id="sidebar" class="sidebar sidebar-transparent">
            <!-- begin sidebar scrollbar -->
            <div data-scrollbar="true" data-height="100%">
                <!-- begin sidebar user -->

                <ul class="nav">
                    <?php if(isset(Yii::$app->user->identity)) { ?>
                    <li class="nav-profile">
                        <a href="javascript:;" data-toggle="nav-profile">
                            <div class="cover with-shadow"></div>
                            <div class="image">
                                <img src="<?=Yii::$app->user->identity->getAvatar() ?>" alt="" />
                            </div>
                            <div class="info">
                                <b class="caret pull-right"></b>
                                <?=Yii::$app->user->identity->fio?>
                                <small><?=Yii::$app->user->identity->getTypeDescription()?></small>
                            </div>
                        </a>
                    </li>
                    <?php } ?>
                    <li>
                        <ul class="nav nav-profile">
                            <li><?= Html::a('<i class="fa fa-cogs"></i> Изменит Профиль', ['/users/profile'], []); ?></li>
                            <li><?= Html::a(
                                    '<i class="fa fa-sign-out"></i>Выйти',
                                    ['/site/logout'], 
                                    ['data-method' => 'post',]   
                                ) ?></li>
                        </ul>
                    </li>
                </ul>
                <!-- end sidebar user -->
                <!-- begin sidebar nav -->
                
                <?= Menu::widget(
                    [
                        'options' => ['class' => 'nav'],
                        'items' => [
                            //['label' => 'Меню', 'options' => ['class' => 'nav-header']],
                            //['label' => 'Домашняя страница', 'icon' => 'dashboard', 'url' => ['/site'], 'active' => $this->context->id == 'site'],
                            ['label' => 'Заказы', 'icon' => 'cubes', 'url' => ['/orders'], 'active' => $pathInfo == 'orders',
                                'visible' => Yii::$app->user->identity ? (Yii::$app->user->identity->type != 3 ? true : false) : false, ],
                            ['label' => 'Расчет стоимости', 'icon' => 'cube', 'url' => ['/orders/create'], 'active' => $pathInfo == 'orders/create'],
                            ['label' => 'Справочники', 'icon' => 'book', 'url' => ['#'], 
                             'visible' => Yii::$app->user->identity ? (Yii::$app->user->identity->type != 3 ? true : false) : false,
                                'items' => [
                                    ['label' => 'ЛДСП', 'url' => ['/ldsp',], 'active' => $pathInfo == 'ldsp' ],
                                    ['label' => 'Задняя стенка', 'url' => ['/ldsp/orgalit',], 'active' => $pathInfo == 'ldsp/orgalit'],
                                    ['label' => 'Кромка', 'url' => ['/edge'], 'active' => $this->context->id == 'edge'],
                                    ['label' => 'Допы', 'url' => ['#'],
                                        'items' => [
                                            ['label' => 'Эскизы', 'url' => ['/additional/eskiz'], 'active' => $pathInfo == 'additional/eskiz'],
                                            ['label' => 'Фрезеровка', 'url' => ['/additional/frezerovka'], 'active' => $pathInfo == 'additional/frezerovka'],
                                            ['label' => 'Присадка', 'url' => ['/additional/prisadka'], 'active' => $pathInfo == 'additional/prisadka'],
                                        ],
                                    ],
                                    ['label' => 'Пользователи', 'url' => ['/users'], 'active' => $this->context->id == 'users'],
                                    ['label' => 'Настройка', 'url' => ['/about-company/view'], 'active' => $this->context->id == 'about-company'],
                                ],
                            ],
                        ],
                    ]
                ) ?>
            <li style="list-style: none;"><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
                <!-- end sidebar nav -->
            </div>
            <!-- end sidebar scrollbar -->
        </div>
        <div class="sidebar-bg"></div>