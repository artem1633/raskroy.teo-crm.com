<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ldsp */
?>
<div class="ldsp-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
