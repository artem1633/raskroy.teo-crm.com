<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Additional */
?>
<div class="additional-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
