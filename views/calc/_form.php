<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php if(Yii::$app->user->identity == null) { ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php } ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

    <?=$form->field($model, 'resume')->fileInput([]) ?>

    <?=$form->field($model, 'check')->checkBox(['label'=>'<a href="'.Yii::$app->params['targetBlankSite'].
            '" target="_blank">Согласен на обработку персональных данных</a>']) ?>
        

  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
