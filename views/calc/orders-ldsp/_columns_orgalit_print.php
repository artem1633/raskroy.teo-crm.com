<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\touchspin\TouchSpin;
use app\models\OrdersLdsp;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header'=>'№',
        'width' => '15px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ldsp_id',
        'label'=>'Цвет',
        'width'=>'200px',
        'content' => function($data){
            return $data->ldsp->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'width',
        'width'=>'100px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'height',
        'width'=>'100px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
        'width' => '30px',
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'width' => '40px',
        'format'=>'html',
        'content' => function($data){
        return $data->downloadFile();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'width' => '200px',
        'format'=>'html',
    ],
];   