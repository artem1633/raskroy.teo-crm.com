<?php

use yii\widgets\DetailView;
use yii\helpers\Html;
use kartik\touchspin\TouchSpin;
use app\models\Ldsp;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;

?>
 
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h3 class="panel-title">Общая информация</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 ">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'user_id',
                            'name',
                            'phone',
                            'email:email',
                            [
                                'attribute'  => 'file',
                                'format'=>'raw',
                                'value'=> $model->downloadFile(),
                            ],
                            'comment:ntext',
                            'price',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>


<div class="panel panel-inverse">
    <div class="panel-heading">
        <h3 class="panel-title">ЛДСП</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 ">
                <?=GridView::widget([
                   'dataProvider' => $OrderLdsp,
                   'pjax'=>true,
                   'columns' => require(__DIR__.'/orders-ldsp/_columns_print.php'),
                   'striped' => true,
                   'summary' => false,
                   'condensed' => true,
                   'responsive' => true,
                ])?>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h3 class="panel-title">Оргалит</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 ">
            <?=GridView::widget([
                'dataProvider' => $OrderOrgalit,
                'pjax'=>true,
                'columns' => require(__DIR__.'/orders-ldsp/_columns_orgalit_print.php'),
                'striped' => true,
                'summary' => false,
                'condensed' => true,
                'responsive' => true,
            ])?>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h3 class="panel-title">Эскизы (общее количество)</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-condensed">
                    <tbody>
                        <tr>
                            <?php foreach ($additionals as $value) { 
                                if($value->additional->type_additional == 'Эскизы') {
                            ?>
                                <th style="text-align:center;"><?=$value->additional->name?></th>
                            <?php } } ?>
                        </tr>
                        <tr>
                            <?php foreach ($additionals as $value) { 
                                if($value->additional->type_additional == 'Эскизы') {
                            ?>
                                <td><?=$value->count?></td>
                            <?php } } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h3 class="panel-title">Фрезеровка (общее количество)</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-condensed">
                    <tbody>
                        <tr>
                            <?php foreach ($additionals as $value) { 
                                if($value->additional->type_additional == 'Фрезеровка') {
                            ?>
                                <th style="text-align:center;"><?=$value->additional->name?></th>
                            <?php } } ?>
                        </tr>
                        <tr>
                            <?php foreach ($additionals as $value) { 
                                if($value->additional->type_additional == 'Фрезеровка') {
                            ?>
                                <td><?=$value->count?></td>
                            <?php } } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-heading">
        <h3 class="panel-title">Присадка (общее количество)</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped table-condensed">
                    <tbody>
                        <tr>
                            <?php foreach ($additionals as $value) { 
                                if($value->additional->type_additional == 'Присадка') {
                            ?>
                                <th style="text-align:center;"><?=$value->additional->name?></th>
                            <?php } } ?>
                        </tr>
                        <tr>
                            <?php foreach ($additionals as $value) { 
                                if($value->additional->type_additional == 'Присадка') {
                            ?>
                                <td><?=$value->count?></td>
                            <?php } } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-inverse">
    <div class="panel-body bg-warning">
        <span style="font-weight: bold; font-size: 18px;">Итоговая стоимость:</span> 
        <span style="margin-right: 20px; font-size: 18px; color: red;" id="total_price"><?=$model->price?> руб.</span>
        <?= Html::a('Распечетать <i class="fa fa-print"></i>', ['print', 'id' => $model->id, 'type' => 1], ['data-pjax'=>'0', 'target' => '_blank', 'title'=> 'Распечетать', 'class'=>'btn btn-warning']) ?>
    </div>
</div>
