<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Edge */
?>
<div class="edge-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
