<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr>
        <td colspan="9" style=" text-align: left; font-size: 20px; padding-left: 10px;">ЛДСП</td>
    </tr>
    <thead>
        <tr>
            <th style="text-align:center; padding:3px;">№ детали</th>
            <th style="text-align:center; padding:3px;">Цвет</th>
            <th style="text-align:center; padding:3px;">Толщина</th>
            <th style="text-align:center; padding:3px;">Длина</th>
            <th style="text-align:center; padding:3px;">Ширина</th>
            <th style="text-align:center; padding:3px;">Кол-во</th>
            <th style="text-align:center; padding:3px;">Кромка по длине</th>
            <th style="text-align:center; padding:3px;">Кромка по ширине</th>
            <th style="text-align:center; padding:3px;">Примечание</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach ($OrderLdsp->getModels() as $model) { $i++; ?>
            <tr>
                <td style="text-align:left; padding:3px;"><?=$i?></td>
                <td style="text-align:left; padding:3px;"><?=$model->ldsp->name?></td>
                <td style="text-align:left; padding:3px;"><?=$model->ldsp->thickness?></td>
                <td style="text-align:left; padding:3px;"><?=$model->width?></td>
                <td style="text-align:left; padding:3px;"><?=$model->height?></td>
                <td style="text-align:left; padding:3px;"><?=$model->count?></td>
                <td style="text-align:left; padding:3px;"><?=$model->edgeHeightLeft->name . ' - ' . $model->edgeHeightRight->name?></td>
                <td style="text-align:left; padding:3px;"><?=$model->edgeWidthLeft->name . ' - ' . $model->edgeWidthRight->name?></td>
                <td style="text-align:left; padding:3px;"><?=$model->comment?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr>
        <td colspan="6" style=" text-align: left; font-size: 20px; padding-left: 10px;">Задняя стенка</td>
    </tr>
    <thead>
        <tr>
            <th style="text-align:center; padding:3px;">№ детали</th>
            <th style="text-align:center; padding:3px;">Цвет</th>
            <th style="text-align:center; padding:3px;">Длина</th>
            <th style="text-align:center; padding:3px;">Ширина</th>
            <th style="text-align:center; padding:3px;">Кол-во</th>
            <th style="text-align:center; padding:3px;">Примечание</th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach ($OrderOrgalit->getModels() as $model) { $i++; ?>
            <tr>
                <td style="text-align:left; padding:3px;"><?=$i?></td>
                <td style="text-align:left; padding:3px;"><?=$model->ldsp->name?></td>
                <td style="text-align:left; padding:3px;"><?=$model->width?></td>
                <td style="text-align:left; padding:3px;"><?=$model->height?></td>
                <td style="text-align:left; padding:3px;"><?=$model->count?></td>
                <td style="text-align:left; padding:3px;"><?=$model->comment?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="3" style=" text-align: left; font-size: 20px; padding-left: 10px;">Эскизы (общее количество)</td>
    </tr>
    <thead>
        <tr>
            <?php foreach ($additionals as $value) { 
                if($value->additional->type_additional == 'Эскизы') {
            ?>
                <th style="text-align:center; padding:3px;"><?=$value->additional->name?></th>
            <?php } } ?>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php foreach ($additionals as $value) { 
                if($value->additional->type_additional == 'Эскизы') {
            ?>
                <td style="text-align:left; padding:3px;"><?=$value->count?></td>
            <?php } } ?>
        </tr>
    </tbody>
</table>

<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="4" style=" text-align: left; font-size: 20px; padding-left: 10px;">Фрезеровка (общее количество)</td>
    </tr>
    <thead>
        <tr>
            <?php foreach ($additionals as $value) { 
                if($value->additional->type_additional == 'Фрезеровка') {
            ?>
                <th style="text-align:center; padding:3px;"><?=$value->additional->name?></th>
            <?php } } ?>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php foreach ($additionals as $value) { 
                if($value->additional->type_additional == 'Фрезеровка') {
            ?>
                <td style="text-align:left; padding:3px;"><?=$value->count?></td>
            <?php } } ?>
        </tr>
    </tbody>
</table>

<br>
<br>
<table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
    <tr >
        <td colspan="3" style=" text-align: left; font-size: 20px; padding-left: 10px;">Присадка (общее количество)</td>
    </tr>
    <thead>
        <tr>
            <?php foreach ($additionals as $value) { 
                if($value->additional->type_additional == 'Присадка') {
            ?>
                <th style="text-align:center; padding:3px;"><?=$value->additional->name?></th>
            <?php } } ?>
        </tr>
    </thead>
    <tbody>
        <tr>
            <?php foreach ($additionals as $value) { 
                if($value->additional->type_additional == 'Присадка') {
            ?>
                <td style="text-align:left; padding:3px;"><?=$value->count?></td>
            <?php } } ?>
        </tr>
    </tbody>
</table>

<br><br>
<span style="font-weight: bold; font-size: 18px;">Итоговая стоимость : </span> 
<span style="margin-right: 20px; font-size: 18px; color: red;" id="total_price"><?=$price?> руб.</span>