<?php
use yii\helpers\Url;
use yii\helpers\Html;
use kartik\touchspin\TouchSpin;
use app\models\OrdersLdsp;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header'=>'№',
        'width' => '15px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ldsp_id',
        'label'=>'(Толщина) Цвет',
        'width'=>'150px',
        'content' => function($data){
            return '<b>('. $data->ldsp->thickness . ')</b> ' . $data->ldsp->name;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'width',
        'width'=>'85px',
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => $data->width,
                'id'=>'width'.$data->id,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style'=>'',
                    'onchange'=>" $.get('/orders-ldsp/set-values', {'id':$data->id, 'attribute': 'width', 'value':$(this).val()}, function(data){ 
                        document.getElementById('total_price').innerText = data;
                    } );  $('#width{$data->id}').inputmask('decimal',{max:2780});",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'height',
        'width'=>'85px',
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'data',
                'value' => $data->height,
                'id'=>'height'.$data->id,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style'=>'',
                    'onchange'=>"$.get('/orders-ldsp/set-values', {'id':$data->id, 'attribute': 'height', 'value':$(this).val()}, function(data){ 
                        document.getElementById('total_price').innerText = data;
                    } ); $('#height{$data->id}').inputmask('decimal',{max:2050}); ",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false,]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
        'width'=>'30px',
        'content' => function($data){
            return \yii\widgets\MaskedInput::widget([
                'name' => 'count'.$data->id,
                'value' => $data->count,
                'id' => 'count'.$data->id,
                'mask' => '9',
                'options' => [
                    'class' =>'form-control',
                    'style'=>'',
                    'onchange'=>"$.get('/orders-ldsp/set-values', {'id':$data->id, 'attribute': 'count', 'value':$(this).val()}, function(data){ 
                        document.getElementById('total_price').innerText = data;
                    } );",
                ],
                'clientOptions' => ['repeat' => 10, 'greedy' => false,]
            ]);
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'edge_width_left_id',
        'width' => '150px',
        'format'=>'raw',
        'content' => function($data){
            return '<div class="row">' . Html::dropDownList('edge_width_left_id', $data->edge_width_left_id, OrdersLdsp::getEdgeList(), [
                'class'=>'drop-down-columns',
                'id' => 'edge_width_left_id'.$data->id,
                'style' => 'width:70px',
                'onchange'=>' var a = $( "#edge_width_left_id'.$data->id.'" ).val();
                    $.post( "/orders-ldsp/set-values?id='.$data->id.'&attribute=edge_width_left_id&value="+a, function( data ){ 
                        document.getElementById("total_price").innerText = data;
                     });
            '] )."&nbsp ".
            Html::dropDownList('edge_width_right_id', $data->edge_width_right_id, OrdersLdsp::getEdgeList(), [
                'class'=>'drop-down-columns',
                'id' => 'edge_width_right_id'.$data->id,
                'style' => 'width:70px',
                'onchange'=>' var a = $( "#edge_width_right_id'.$data->id.'" ).val();
                    $.post( "/orders-ldsp/set-values?id='.$data->id.'&attribute=edge_width_right_id&value="+a, function( data ){ document.getElementById("total_price").innerText = data; });
            '] ) . '</div>';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'edge_height_left_id',
        'width' => '150px',
        'format'=>'raw',
        'content' => function($data){
            return '<div class="row">' . Html::dropDownList('edge_height_left_id', $data->edge_height_left_id,  OrdersLdsp::getEdgeList(), [
                'class'=>'drop-down-columns',
                'id' => 'edge_height_left_id'.$data->id,
                'style' => 'width:70px',
                'onchange'=>' var a = $( "#edge_height_left_id'.$data->id.'" ).val();
                    $.post( "/orders-ldsp/set-values?id='.$data->id.'&attribute=edge_height_left_id&value="+a, function( data ){ 
                        document.getElementById("total_price").innerText = data;
                     });
            '] )."&nbsp ".
            Html::dropDownList('edge_height_right_id', $data->edge_height_right_id,  OrdersLdsp::getEdgeList(), [
                'class'=>'drop-down-columns',
                'id' => 'edge_height_right_id'.$data->id,
                'style' => 'width:70px',
                'onchange'=>' var a = $( "#edge_height_right_id'.$data->id.'" ).val();
                    $.post( "/orders-ldsp/set-values?id='.$data->id.'&attribute=edge_height_right_id&value="+a, function( data ){ 
                        document.getElementById("total_price").innerText = data;
                     });
            '] ) . '</div>';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'file',
        'width' => '40px',
        'format'=>'html',
        'content' => function($data){
        return "<div style='width:30px; height:30px;'>
                    <input type='hidden' name='uname' id='uname{$data->id}' value='{$data->id}'>
                    <label for='inputFile_submit{$data->id}' class='btn'><i class='glyphicon glyphicon-circle-arrow-down' style='color:blue;margin-left:-5px;' title='Загрузить файл'></i></label>
                    <input id='inputFile_submit{$data->id}' name='file' style='visibility:hidden;' type='file'
                    onchange='uploadFileOrders({$data->id}); ' >
                </div>".$data->downloadFile();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'comment',
        'width' => '150px',
        'format'=>'html',
        'content' => function($data){
            return"<input id='comment{$data->id}' type='text' value='{$data->comment}' class='form-control comment_tab' 
                onchange=' var a = $(\"#comment{$data->id}\").val();
                $.post( \"/orders-ldsp/set-values?id={$data->id}&attribute=comment&value=\"+a, function( data ){
                 });'>";
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'width' => '15px',
        'header'=>'#',
        'template' => '{leadDelete}',
        'buttons'  => [
            'leadDelete' => function ($url, $model) {
                return "<i class=' btn btn-danger btn-xs glyphicon glyphicon-trash ' style='cursor:pointer;' title='Удалить'
                    onclick='
                        $.get( \"/orders-ldsp/delete?id={$model->id}\", function( data ){ 
                            document.getElementById(\"total_price\").innerText = data;
                             $.pjax.reload({container:\"#crud-datatable\", async: false});
                         });
                        $.pjax.reload({container:\"#crud-datatable\", async: false});
                        '
                ></i>";
            },
        ],
    ],

];   