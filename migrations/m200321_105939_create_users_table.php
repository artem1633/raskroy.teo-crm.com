<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200321_105939_create_users_table extends Migration
{
     public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->comment("ФИО"),
            'login' => $this->string(255)->comment("Логин"),
            'password' => $this->string(255)->comment("Пароль"),
            'type' => $this->integer()->comment("Роль"),
            'phone' => $this->string(255)->comment("Телефон номер"),
            'email' => $this->string(255)->comment("Email"),
            'avatar' => $this->string(255)->comment("Аватар пользователя"),
        ]);

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',
            'login' => 'admin',
            'email' => 'info@raspil-ldsp.pro',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
            'type' => 1,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
