<?php

namespace app\controllers;

use Yii;
use app\models\OrdersLdsp;
use app\models\search\OrdersLdspSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Ldsp;
use app\models\Temporaries;

class OrdersLdspController extends Controller
{   
    public function actionSetLdsp( )
    {
        if(isset($_POST['ldsp']) && $_POST['ldsp'] != null) {
            $model = new OrdersLdsp();
            $model->ldsp_id = $_POST['ldsp'];
            $model->temporary_id = $_POST['temporary_id'];
            $model->save();
        }
    }

    public function actionLdspName($id = null)
    {  
        $datas = Ldsp::findOne($id);
        if($datas != null) {
            $ldsp_name = Ldsp::find()->where(['thickness' => $datas->thickness])->orderBy(['sorting'=>SORT_DESC])->all();
            foreach ($ldsp_name as $value) { 
                echo "<option value = '".$value->id."'>".$value->name."</option>" ;            
            }            
        }
    }

    public function actionSetValues($id , $attribute , $value)
    {
        $model = OrdersLdsp::findOne($id);
        if($attribute == 'width') { 
            if($value >=0 && $value <= 2780) $model->width = $value; 
            elseif($value > 2780) $model->width = 2780;
            else  $model->width = 0;
        }

        if($attribute == 'height') { 
            if($value >=0 && $value <= 2050) $model->height = $value; 
            elseif($value > 2050) $model->height = 2050;
            else  $model->height = 0;
        }

        // if($attribute == 'height') { 
        //     if($value >=0 ) $model->height = $value;   else $model->height = 0;
        // }

        if($attribute == 'count') {
            if($value >= 0 )$model->count = $value; else $model->count = 0;
        }
        if($attribute == 'edge_width_left_id') $model->edge_width_left_id = (integer)$value;
        if($attribute == 'edge_width_right_id') $model->edge_width_right_id = (integer)$value;
        if($attribute == 'edge_height_left_id') $model->edge_height_left_id = (integer)$value;
        if($attribute == 'edge_height_right_id') $model->edge_height_right_id = (integer)$value;
        if($attribute == 'comment') $model->comment = $value;
        
        $model->save();
        $price = $model->temporary->setPrice();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $price . ' руб';
    }

    public function actionSetTabJs()
    {   
        if(isset($_POST['type']) && isset($_POST['temporary_id']) && $_POST['temporary_id'] != null) {
            $model = OrdersLdsp::find()
                                ->innerJoin('ldsp' , 'ldsp.id=orders_ldsp.ldsp_id')
                                ->where(['type' =>$_POST['type'] ])
                                ->andWhere(['temporary_id' => $_POST['temporary_id']])
                                ->orderBy(['id' => SORT_DESC])
                                ->one();


            $orderLdsp = new OrdersLdsp();
            $orderLdsp->attributes = $model->attributes;
            $orderLdsp->check_save = 1;
            $orderLdsp->save();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return true;
        }
    }

     public function actionSendFile()
    {   
        $uname = $_POST['uname'];
        $model = OrdersLdsp::findOne($uname);

        if($_FILES['file'])
        {   
            if(file_exists('uploads/orders_ldsp/'.$model->file) && $model->file != null)
            {
                unlink('uploads/orders_ldsp/'.$model->file);
            }

            $uploadDir = \Yii::getAlias('@app').'/web/uploads/orders_ldsp/';
            $ext = "";
            $ext = substr(strrchr($_FILES['file']['name'], "."), 1); 

            $fPath =$model->id."-".Yii::$app->security->generateRandomString().$_FILES['file']['name'];
            if($ext != ""){
                $result = move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir . $fPath);
                $model->file = $fPath;
                $model->save();
            }
            //return $this->redirect(['/orders/temporary', 'id' => $model->temporary_id]);
        }
    }

    public function actionDownloadFile($file)
    {   
        if(file_exists('uploads/orders_ldsp/'.$file )&&$file != null)
        {
            return Yii::$app->response->sendFile(Yii::getAlias('uploads/orders_ldsp/'.$file));
        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = OrdersLdsp::findOne($id);
        $ldsp = OrdersLdsp::find()->where(['file' => $model->file])->all();
        if($model != null && $ldsp == null){
            if(file_exists('uploads/orders_ldsp/'.$model->file )&&$model->file != null)
            {
                unlink('uploads/orders_ldsp/'.$model->file );
            }
        }
        $model->delete();
        $temporary = Temporaries::findOne($model->temporary_id);
        $price = $temporary->setPrice();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $price . ' руб';
    }

    protected function findModel($id)
    {
        if (($model = OrdersLdsp::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
