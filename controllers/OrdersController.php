<?php

namespace app\controllers;

use Yii;
use app\models\Orders;
use app\models\search\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use app\models\Temporaries;
use app\models\OrdersAdditional;
use app\models\Ldsp;
use app\models\OrdersLdsp;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use yii\web\UploadedFile;
use yii\base\ErrorException;
use app\models\Users;

class OrdersController extends Controller
{
   
    public function behaviors()
    {
        return [
            // 'access' => [
            //     'class' => \yii\filters\AccessControl::className(),
            //     'only' => ['index'],
            //     'rules' => [
            //         [
            //             'actions' => ['index'],
            //             'allow' => true,
            //             'roles' => ['@'],
            //         ],
            //     ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionView($id)
    {   
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $additionals = OrdersAdditional::find()->where(['order_id' => $id])->all();

        $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $id, 'ldsp.type' => 'ЛДСП']);
        $OrderLdsp = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $id, 'ldsp.type' => 'Оргалит']);
        $OrderOrgalit = new ActiveDataProvider([
            'query' => $query,
        ]);

        

        return $this->render('view', [
            'model' => $this->findModel($id),
            'additionals' => $additionals,
            'OrderLdsp' => $OrderLdsp,
            'OrderOrgalit' => $OrderOrgalit,
        ]);
    }

    public function actionIndex()
    {    
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateOrder($temporary_id)
    {
        $request = Yii::$app->request;
        $model = new Orders();
        $user = Yii::$app->user->identity;
        if($user != null) $model->type = 1;
        else $model->type = 2;

        if($request->isAjax){

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()) {

                $temporary = Temporaries::findOne($temporary_id);
                $additionals = OrdersAdditional::find()->where(['temporary_id' => $temporary->id])->all();
                $OrderLdsp = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $temporary->id,])->all();
                foreach ($OrderLdsp as $value) {
                    $value->order_id = $model->id;
                    $value->temporary_id = null;
                    $value->save();
                }

                foreach ($additionals as $value) {
                    $value->order_id = $model->id;
                    $value->temporary_id = null;
                    $value->save();
                }

                $model->price = $temporary->price;
                $model->save();
                $model->resume = UploadedFile::getInstance($model, 'resume');
                $model->uploadResume();
                // ***************** PDF GENEREATE  SEND TO GAMIL ************************
                $price = 0; $file_name = "";
                
                $price = $model->price;
                $additionals = OrdersAdditional::find()->where(['order_id' => $model->id])->all();

                $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $model->id, 'ldsp.type' => 'ЛДСП']);
                $OrderLdsp = new ActiveDataProvider([
                    'query' => $query,
                ]);

                $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $model->id, 'ldsp.type' => 'Оргалит']);
                $OrderOrgalit = new ActiveDataProvider([
                    'query' => $query,
                ]);

                $file_name = $model->email;

                Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
                $headers = Yii::$app->response->headers;
                $headers->add('Content-Type', 'application/pdf');

                $content = $this->renderPartial('orders-ldsp/print-html',[
                    'model' => $model,
                    'price' => $price,
                    'additionals' => $additionals,
                    'OrderLdsp' => $OrderLdsp,
                    'OrderOrgalit' => $OrderOrgalit,
                ]);
                // setup kartik\mpdf\Pdf component
                $pdf = new Pdf([
                    'filename' => $file_name.'.pdf',
                    // set to use core fonts only
                    'mode' => Pdf::MODE_UTF8, 
                    // A4 paper format
                    'format' => Pdf::FORMAT_A4, 
                    // portrait orientation
                    'orientation' => Pdf::ORIENT_PORTRAIT, 
                    // stream to browser inline
                    'destination' => Pdf::DEST_BROWSER, 
                    // your html content input
                    'content' => $content,  
                    // format content from your own css file if needed or use the
                    // enhanced bootstrap css built by Krajee for mPDF formatting 
                    'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                    // any css to be embedded if required
                    'cssInline' => '.kv-heading-1{font-size:18px}', 
                     // set mPDF properties on the fly
                    'options' => ['title' => 'Krajee Report Title'],
                     // call mPDF methods on the fly
                    'methods' => [ 
                        'SetHeader'=>['ПЕЧАТЬ РАСЧЕТА'], 
                        'SetFooter'=>['{PAGENO}'],
                    ]
                ]);
                // ****************************** END PDF GENERATE *************************
                $fileName = "";
                $fileName = $pdf->filename;
                $filePath = Yii::getAlias('@app/web/uploads/pdf/' . $fileName);

                $attachment =  $pdf->output($content, $filePath, Pdf::DEST_FILE);

                $model->sendSmsToEmail($filePath);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{           
                return [
                    'title'=> "Отправить заявку",
                    'content'=>$this->renderAjax('_form', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','id'=>'submit_bt' ,'data-dismiss'=>"modal"]).
                                Html::button('Заказать',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('_form', [
                    'model' => $model,
                ]);
            }
        }
       
    }
    
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Temporaries();
        $model->save();
        return $this->redirect(['/orders/temporary', 'id' => $model->id]);
    }

    public function actionTemporary($id)
    {       
         // $model = OrdersLdsp::find()
         //                        ->innerJoin('ldsp' , 'ldsp.id=orders_ldsp.ldsp_id')
         //                        ->where(['type' => 'ЛДСП'])
         //                        ->andWhere(['temporary_id' => 19])
         //                        ->orderBy(['id' => SORT_DESC])
         //                        ->one();

         //                        echo "<pre>";
         //                        print_r($model); die;
        $request = Yii::$app->request;
        $model = Temporaries::findOne($id);
        $additionals = OrdersAdditional::find()->where(['temporary_id' => $model->id])->all();

        $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $model->id, 'ldsp.type' => 'ЛДСП']);
        $OrderLdsp = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $model->id, 'ldsp.type' => 'Оргалит']);
        $OrderOrgalit = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('create', [
            'model' => $model,
            'additionals' => $additionals,
            'OrderLdsp' => $OrderLdsp,
            'OrderOrgalit' => $OrderOrgalit,
        ]);
    }

    public function actionPrintTemporary($temporary_id)
    {
        $request = Yii::$app->request;
        $model = Temporaries::findOne($temporary_id);
        $additionals = OrdersAdditional::find()->where(['temporary_id' => $model->id])->all();

        $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $model->id, 'ldsp.type' => 'ЛДСП']);
        $OrderLdsp = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $model->id, 'ldsp.type' => 'Оргалит']);
        $OrderOrgalit = new ActiveDataProvider([
            'query' => $query,
        ]);

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title'=> "Печать",
            'size' => 'large',
            'content'=>$this->renderAjax('print', [
                'model' => $model,
                'additionals' => $additionals,
                'OrderLdsp' => $OrderLdsp,
                'OrderOrgalit' => $OrderOrgalit,
            ]),
        ];       
    }

    public function actionPrint($id, $type = 2) {
        $price = 0; $file_name = "";
        if($type == 2) {
            $model = Temporaries::findOne($id);
            $price = $model->price;
            $additionals = OrdersAdditional::find()->where(['temporary_id' => $model->id])->all();

            $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $model->id, 'ldsp.type' => 'ЛДСП']);
            $OrderLdsp = new ActiveDataProvider([
                'query' => $query,
            ]);

            $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $model->id, 'ldsp.type' => 'Оргалит']);
            $OrderOrgalit = new ActiveDataProvider([
                'query' => $query,
            ]);

            $file_name = "printing_calculation";
        }
        if($type == 1) {
            $model = Orders::findOne($id);
            $price = $model->price;
            $additionals = OrdersAdditional::find()->where(['order_id' => $model->id])->all();

            $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $model->id, 'ldsp.type' => 'ЛДСП']);
            $OrderLdsp = new ActiveDataProvider([
                'query' => $query,
            ]);

            $query = OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $model->id, 'ldsp.type' => 'Оргалит']);
            $OrderOrgalit = new ActiveDataProvider([
                'query' => $query,
            ]);

            $file_name = $model->email;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');

        $content = $this->renderPartial('orders-ldsp/print-html',[
            'model' => $model,
            'price' => $price,
            'additionals' => $additionals,
            'OrderLdsp' => $OrderLdsp,
            'OrderOrgalit' => $OrderOrgalit,
        ]);
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'filename' => $file_name.'pdf',
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['ПЕЧАТЬ РАСЧЕТА'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    // --------------------------- ajax value yuklash orders_additional jadvaliga ----------------------
    public function actionSetValues($temporary_id, $additional_id, $value)
    {
        $model = OrdersAdditional::find()
                ->where(['temporary_id'=>$temporary_id])
                ->andWhere(['id' => $additional_id])
                ->one();
        if($model != null){
            $model->count = $value;
            $model->save();
            
            $temporary = Temporaries::findOne($temporary_id);
            $price = $temporary->setPrice();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return $price . ' руб';

        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if ($id != 1)$this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }


    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSendFile($file)
    {   
        return Yii::$app->response->sendFile(Yii::getAlias('uploads/users/'.$file));
    }
}
