<?php

namespace app\models;


use Yii;
use yii\helpers\ArrayHelper;
use app\models\constants\UsersConstants;


/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property string $login Логин
 * @property string $password Пароль
 * @property int $type Роль
 * @property string $phone Телефон номер
 * @property string $email Email
 * @property string $avatar Аватар пользователя
 *
 * @property Orders[] $orders
 */

class Users extends \yii\db\ActiveRecord
{
    public $new_password;
    public $image;

   
    public static function tableName()
    {
        return 'users';
    }

    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'type', 'email'], 'required'],
            [['login'],  'unique'],
            [['email'],  'email'],
            [['image','new_password'],'safe'],
            [['fio', 'login', 'password', 'phone', 'email', 'avatar'], 'string', 'max' => 255],
        ];
    }

 
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'type' => 'Роль',
            'phone' => 'Телефон номер',
            'email' => 'Email',
            'avatar' => 'Фото',
            'image' => 'Фото',
            'new_password' => 'Новый пароль',
           
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $user = Yii::$app->user->identity;
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        if($this->new_password != null) $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        return parent::beforeSave($insert);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function getTypeDescription()
    {
        return UsersConstants::getString($this->type);
    }

    public function getType()
    {
        return UsersConstants::getArray();
    }

    public function getAvatar()
    {
        return UsersConstants::getImage($this->avatar);
    }

    public function upload()
    {   
        $fileName = "";
        $fileName = $this->id . '-' . Yii::$app->security->generateRandomString() . '.' . $this->image->extension;
        if(!empty($this->image))
        {   
            if(file_exists('uploads/avatars/'.$this->avatar) && $this->avatar != null)
            {
                unlink('uploads/avatars/'.$this->avatar);
            }

            $this->image->saveAs('uploads/avatars/'.$fileName);
            Yii::$app->db->createCommand()->update('users', ['avatar' => $fileName], [ 'id' => $this->id ])->execute();
        }
    }
}
