<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\base\ErrorException;
use app\models\OrdersLdsp;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $type Тип
 * @property string $name Имя
 * @property string $phone Телефон
 * @property string $email Email
 * @property string $file Файл
 * @property string $comment Комментария
 * @property double $price  Итоговая цена
 *
 * @property Users $user
 * @property OrdersAdditional[] $ordersAdditionals
 * @property OrdersLdsp[] $ordersLdsps
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $resume;
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type' ,'check'], 'integer'],
            [['comment'], 'string'],
            [['resume', 'date_cr'], 'safe'],
            [['email'], 'email'],
            [['price'], 'number'],
            [['name', 'phone', 'email', 'file'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['name', 'required', 'when' => function($model) { return $model->type == 2;}, 'enableClientValidation' => false],
            ['phone', 'required', 'when' => function($model) { return $model->type == 2;}, 'enableClientValidation' => false],
            // ['email', 'required', 'when' => function($model) { return $model->type == 2;}, 'enableClientValidation' => false],
            ['check', 'required',  'requiredValue' => 1, 'message' => 'Необходимо заполнить «Согласен на обработку персональных данных».'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'type' => 'Тип', // 1 - user, 2 - nouser
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'file' => 'Файл',
            'comment' => 'Комментария',
            'price' => 'Итоговая цена',
            'resume' => 'Резюме пользователя (файл)',
            'date_cr' => 'Дата создание',
            'check' => '<a href="'.Yii::$app->params['targetBlankSite'].'" target="_blank">Согласен на обработку персональных данных</a>',
            // '.[Yii::$app->params['targetBlankSite'].'
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_cr = date('Y-m-d H:i:s');
            $user = Yii::$app->user->identity;
            if($user != null) {
                $this->user_id = $user->id;
                $this->type = 1;
                $this->name = $user->fio;
                $this->phone = $user->phone;
                $this->email = $user->email;
            }
            else {
                $this->type = 2;
            }
        }
        return parent::beforeSave($insert);
    }

    public function uploadResume()
    {
        if(!empty($this->resume))
        {   
            if(file_exists('uploads/users/'.$this->file) && $this->file != null)
            {
                unlink('uploads/users/'.$this->file);
            }

            $fileName = time() . '_' . $this->resume->baseName . '.' . $this->resume->extension;
            $this->resume->saveAs('uploads/users/' . $fileName);
            Yii::$app->db->createCommand()->update('orders', ['file' => $fileName], [ 'id' => $this->id ])->execute();
        }
    }

    public function downloadFile()
    {
        if($this->file != null) return Html::a(/*$this->file .*/ 'Скачать<span class="fa fa-arrow-circle-o-down fa-lg"></span>', ['/orders/send-file', 'file' => $this->file],['data-pjax'=>0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAdditionals()
    {
        return $this->hasMany(OrdersAdditional::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps()
    {
        return $this->hasMany(OrdersLdsp::className(), ['order_id' => 'id']);
    }

    public function sendSmsToEmail($filePath)
    {
        
        $admin_email = Users::find()->where(['type' =>1])->select('email')->asArray()->all();
        $array = [];
        foreach ($admin_email as  $value) {
            $array [] = $value['email'];
        }
     
        try {
            $message = Yii::$app->mailer->compose(
                ['html' => 'admin-html', 'text' => 'admin-text'],
                [
                    'model'=>Orders::findOne($this->id),
                    'ldsp' => OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $this->id, 'ldsp.type' => 'ЛДСП'])->all(),
                    'oraglit' => OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $this->id, 'ldsp.type' => 'Оргалит'])->all(),
                    'additionals' => OrdersAdditional::find()->where(['order_id'=>$this->id])->all(),
                 ])
            
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                ->setTo($array)
                ->setSubject('Заявка на расчет стоимости раскроя дсп')
                // ->setHtmlBody('Saloom')
                ->send();
            if($message) {
                Yii::$app->session->setFlash('success','Успешно завершено. Мы отправили информацию на почту');
            }
            else{
                Yii::$app->session->setFlash('warning','Сообщение не был отправлен');
            }
        } catch(\Exception $e) {
            return new ErrorException($e);
        }
        if($this->email != null){
            try {
                $message = Yii::$app->mailer->compose(
                    ['html' => 'orders-html', 'text' => 'orders-text'],
                    [
                        'model'=>Orders::findOne($this->id),
                        'ldsp' => OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $this->id, 'ldsp.type' => 'ЛДСП'])->all(),
                        'oraglit' => OrdersLdsp::find()->joinWith(['ldsp'])->where(['order_id' => $this->id, 'ldsp.type' => 'Оргалит'])->all(),
                        'additionals' => OrdersAdditional::find()->where(['order_id'=>$this->id])->all(),
                     ])
                
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
                    ->attach($filePath)
                    ->setTo($this->email)
                    ->setSubject('Заявка на расчет стоимости раскроя дсп')
                    ->send();
                if($message) {
                    Yii::$app->session->setFlash('success','Успешно завершено. Мы отправили информацию на почту');
                }
                else{
                    Yii::$app->session->setFlash('warning','Сообщение не был отправлен');
                }
            } catch(\Exception $e) {
                return new ErrorException($e);
            }
        }

        unlink($filePath);
    }
}
