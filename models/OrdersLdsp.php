<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Edge;
use yii\helpers\Html;

/**
 * This is the model class for table "orders_ldsp".
 *
 * @property int $id
 * @property int $order_id Заказы
 * @property int $ldsp_id Ldsp
 * @property double $width Длина
 * @property double $height Ширина
 * @property int $count Количество
 * @property int $edge_width_left_id Кромка по длине
 * @property int $edge_width_right_id Кромка по длине
 * @property int $edge_height_left_id Кромка по ширине
 * @property int $edge_height_right_id Кромка по ширине
 * @property int $temporary_id Временно
 * @property string $comment Примечание
 * @property string $file Файл
 *
 * @property Edge $edgeHeightLeft
 * @property Edge $edgeHeightRight
 * @property Edge $edgeWidthLeft
 * @property Edge $edgeWidthRight
 * @property Ldsp $ldsp
 * @property Orders $order
 * @property Temporaries $temporary
 */
class OrdersLdsp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $check_save;
    public static function tableName()
    {
        return 'orders_ldsp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'ldsp_id', 'count', 'edge_width_left_id', 'edge_width_right_id', 'edge_height_left_id', 'edge_height_right_id', 'temporary_id'], 'integer'],
            [['width', 'height'], 'number'],
            [['comment'], 'string'],
            [['check_save'], 'safe'],
            [['file'], 'string', 'max' => 255],
            [['edge_height_left_id'], 'exist', 'skipOnError' => true, 'targetClass' => Edge::className(), 'targetAttribute' => ['edge_height_left_id' => 'id']],
            [['edge_height_right_id'], 'exist', 'skipOnError' => true, 'targetClass' => Edge::className(), 'targetAttribute' => ['edge_height_right_id' => 'id']],
            [['edge_width_left_id'], 'exist', 'skipOnError' => true, 'targetClass' => Edge::className(), 'targetAttribute' => ['edge_width_left_id' => 'id']],
            [['edge_width_right_id'], 'exist', 'skipOnError' => true, 'targetClass' => Edge::className(), 'targetAttribute' => ['edge_width_right_id' => 'id']],
            [['ldsp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ldsp::className(), 'targetAttribute' => ['ldsp_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['temporary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporaries::className(), 'targetAttribute' => ['temporary_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказы',
            'ldsp_id' => 'ЛДСП',
            'width' => 'Длина',
            'height' => 'Ширина',
            'count' => 'Количество',
            'edge_width_left_id' => 'Кромка по длине',
            'edge_width_right_id' => 'Кромка по длине',
            'edge_height_left_id' => 'Кромка по ширине',
            'edge_height_right_id' => 'Кромка по ширине',
            'temporary_id' => 'Временно',
            'comment' => 'Примечание',
            'file' => 'Файл',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {   
            if($this->check_save != 1){
                // $this->count = 0;
                $edge = Edge::find()->one();
                if($edge != null && $this->ldsp->type == 'ЛДСП') {
                    $this->edge_width_left_id = $edge->id;
                    $this->edge_width_right_id = $edge->id;
                    $this->edge_height_left_id = $edge->id;
                    $this->edge_height_right_id = $edge->id;                
                }
            }
        }

        //if($this->new_password != null) $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdgeHeightLeft()
    {
        return $this->hasOne(Edge::className(), ['id' => 'edge_height_left_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdgeHeightRight()
    {
        return $this->hasOne(Edge::className(), ['id' => 'edge_height_right_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdgeWidthLeft()
    {
        return $this->hasOne(Edge::className(), ['id' => 'edge_width_left_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEdgeWidthRight()
    {
        return $this->hasOne(Edge::className(), ['id' => 'edge_width_right_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLdsp()
    {
        return $this->hasOne(Ldsp::className(), ['id' => 'ldsp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemporary()
    {
        return $this->hasOne(Temporaries::className(), ['id' => 'temporary_id']);
    }

    
    public static function getEdgeList()
    {   
        $list = Edge::find()->all();
        return ArrayHelper::map($list, 'id', 'name');
    }

    public function downloadFile()
    {
        if($this->file != null) return Html::a('«Файл»', ['/orders-ldsp/download-file', 'file' => $this->file ], ['data-pjax'=>0,'title'=>'Скачать']);
        else return "";
    }

    public function downloadFileSendEmail()
    {
        if($this->file != null) 
            return '<a href="'.Yii::$app->params['siteName'].'/orders-ldsp/download-file?file='.$this->file.'" data-pjax="0" title="Скачать">«Файл»</a>';
        else return "";
    }

}
