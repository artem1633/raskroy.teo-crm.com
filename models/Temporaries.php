<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temporaries".
 *
 * @property int $id
 * @property int $type Тип
 *
 * @property OrdersAdditional[] $ordersAdditionals
 * @property OrdersLdsp[] $ordersLdsps
 */
class Temporaries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temporaries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'price' => 'Итоговая цена',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->type = 1;
            $this->price = 0;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert) {
            $additionals = Additional::find()->all();
            foreach ($additionals as $value) {
                $ordersAdditional = new OrdersAdditional();
                $ordersAdditional->temporary_id = $this->id;
                $ordersAdditional->additional_id = $value->id;
                // $ordersAdditional->count = 0;
                $ordersAdditional->save();
            }            
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAdditionals()
    {
        return $this->hasMany(OrdersAdditional::className(), ['temporary_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps()
    {
        return $this->hasMany(OrdersLdsp::className(), ['temporary_id' => 'id']);
    }

    public function setPrice()
    {
        $price = 0;
        $additionals = OrdersAdditional::find()->where(['temporary_id' => $this->id])->all();
        $OrderLdsp = OrdersLdsp::find()->joinWith(['ldsp'])->where(['temporary_id' => $this->id])->all();
        foreach ($OrderLdsp as $value) {
            $price += $value->width * $value->height * $value->count * $value->ldsp->cost * 0.000001;
            if($value->edge_width_left_id != null) $price += $value->edgeWidthLeft->cost;
            if($value->edge_width_right_id != null) $price += $value->edgeWidthRight->cost;
            if($value->edge_height_left_id != null) $price += $value->edgeHeightLeft->cost;
            if($value->edge_height_right_id != null) $price += $value->edgeHeightRight->cost;
        }

        foreach ($additionals as $value) {
            $price += $value->additional->cost * $value->count;
        }
        $price = round($price, 2);
        $this->price = $price;
        $this->save();
        return $price;
    }
}
