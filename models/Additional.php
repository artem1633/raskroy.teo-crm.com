<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "additional".
 *
 * @property int $id
 * @property string $name Название
 * @property double $cost Цена
 * @property string $type_additional Тип
 *
 * @property OrdersAdditional[] $ordersAdditionals
 */
class Additional extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'additional';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cost'], 'number'],
            [['name', 'type_additional'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'cost' => 'Цена',
            'type_additional' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersAdditionals()
    {
        return $this->hasMany(OrdersAdditional::className(), ['additional_id' => 'id']);
    }
}
