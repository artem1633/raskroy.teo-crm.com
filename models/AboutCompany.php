<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about_company".
 *
 * @property int $id
 * @property string $logo Логотип
 * @property string $name Наименования компании
 * @property string $address Адрес
 * @property string $phone Телефон
 * @property string $email Email
 * @property string $site Web Сайт
 */
class AboutCompany extends \yii\db\ActiveRecord
{   
     public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {

        return [
            [['address'], 'string'],
            [['image'],'safe'],
            [['name','address'],'required'],
            [['logo', 'name', 'phone', 'email', 'site'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наимование',
            'phone' => 'Телефон',
            'address' => 'Адрес',
            'email' => 'Email',
            'logo' => 'Лого',
            'image'=>'Лого',
            'site' => 'Web Сайт',
        ];
    }

    public function upload()
    {   
        $fileName = $this->id . '-' . Yii::$app->security->generateRandomString() . '.' . $this->image->extension;
        if(!empty($this->image))
        {   
            if(file_exists('uploads/company/'.$this->logo) && $this->logo != null)
            {
                unlink('uploads/company/'.$this->logo);
            }

            $this->image->saveAs('uploads/company/'.$fileName);
            Yii::$app->db->createCommand()->update('about_company', ['logo' => $fileName], [ 'id' => $this->id ])->execute();
        }
    }

     public  function getImage()
    {
        if (!file_exists('uploads/company/'.$this->logo) || $this->logo == '') {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/img/no-logo.png';
        } else {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/uploads/company/'.$this->logo;
        }
        return $path;
    }
}
