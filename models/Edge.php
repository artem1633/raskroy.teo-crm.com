<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "edge".
 *
 * @property int $id
 * @property string $name Название
 * @property double $cost Цена
 *
 * @property OrdersLdsp[] $ordersLdsps
 * @property OrdersLdsp[] $ordersLdsps0
 * @property OrdersLdsp[] $ordersLdsps1
 * @property OrdersLdsp[] $ordersLdsps2
 */
class Edge extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'edge';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cost'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'cost' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps()
    {
        return $this->hasMany(OrdersLdsp::className(), ['edge_height_left_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps0()
    {
        return $this->hasMany(OrdersLdsp::className(), ['edge_height_right_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps1()
    {
        return $this->hasMany(OrdersLdsp::className(), ['edge_width_left_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersLdsps2()
    {
        return $this->hasMany(OrdersLdsp::className(), ['edge_width_right_id' => 'id']);
    }
}
