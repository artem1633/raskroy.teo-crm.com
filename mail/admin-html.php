<?php
use yii\helpers\Html;

?>
<div class="verify-email" style="width:90%; margin-left: 70px;">
    <p style="color:blue; font-size: 15px;">Заявка на расчет стоимости раскроя дсп</p>
    <h4>Тема - заявка на расчет №<?= $model->id?></h4>
    <h4>Поступил новый заказ на расчет стоимости распила</h4>
    <h4>ссылка на заказ:<?= Yii::$app->params['siteName'].'/orders/view?id='.$model->id?> </h4>
    <table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
        <tr >
            <td colspan="9" style=" text-align: left; font-size: 20px; padding-left: 10px;">ЛДСП</td>
        </tr>
        <thead>
            <tr>
                <th  style=" text-align: center;padding:3px;">№ детали</th>
                <th  style=" text-align: center;padding:3px;">Цвет</th>
                <th  style=" text-align: center;padding:3px;">Длина</th>
                <th  style=" text-align: center;padding:3px;">Ширина</th>
                <th  style=" text-align: center;padding:3px;">Количество</th>
                <th  style=" text-align: center;padding:3px;">Кромка по длине</th>
                <th  style=" text-align: center;padding:3px;">Кромка по ширине</th>
                <th  style=" text-align: center;padding:3px;">Примечание</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach ($ldsp as $value):?>
                <tr>
                    <td style=" text-align: left;padding:3px;"><?= $i ?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->ldsp->name?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->width?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->height?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->count?></td>
                    <td style=" text-align: left;padding:3px;">
                        <?= $value->edgeHeightLeft->name?> - <?= $value->edgeHeightRight->name?>
                    </td>
                    <td style=" text-align: left;padding:3px;">
                        <?= $value->edgeWidthLeft->name?> - <?= $value->edgeWidthRight->name?>
                    </td>
                    <td style=" text-align: left;padding:3px;"><?= $value->comment?></td>
                </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
    <br>
    <br>
    <table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
        <tr >
            <td colspan="6" style=" text-align: left; font-size: 20px; padding-left: 10px;">Оргалит</td>
        </tr>
        <thead>
            <tr>
                <th  style=" text-align: center;padding:3px;">№ детали</th>
                <th  style=" text-align: center;padding:3px;">Цвет</th>
                <th  style=" text-align: center;padding:3px;">Длина</th>
                <th  style=" text-align: center;padding:3px;">Ширина</th>
                <th  style=" text-align: center;padding:3px;">Количество</th>
                <th  style=" text-align: center;padding:3px;">Примечание</th>


            </tr>
        </thead>
        <tbody>
            <?php $i = 1; foreach ($oraglit as $value):?>
                <tr>
                    <td style=" text-align: left;padding:3px;"><?= $i ?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->ldsp->name?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->width?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->height?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->count?></td>
                    <td style=" text-align: left;padding:3px;"><?= $value->comment?></td>
                </tr>
            <?php $i++; endforeach; ?>
        </tbody>
    </table>
    <br>
    <br>
    <table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
        <tr >
            <td colspan="3" style=" text-align: left; font-size: 20px; padding-left: 10px;">Эскизы (общее количество)</td>
        </tr>
        <tbody>
            <tr>
                <?php foreach ($additionals as $value) { 
                    if($value->additional->type_additional == 'Эскизы') {
                ?>
                    <th style="text-align:center;"><?=$value->additional->name?></th>
                <?php } } ?>
            </tr>
            <tr>
                <?php foreach ($additionals as $value) { 
                    if($value->additional->type_additional == 'Эскизы') {
                ?>
                    <td><?=$value->count?></td>
                <?php } } ?>
            </tr>
        </tbody>
    </table>

    <br>
    <br>
    <table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
        <tr >
            <td colspan="4" style=" text-align: left; font-size: 20px; padding-left: 10px;">Фрезеровка (общее количество)</td>
        </tr>
        <tbody>
            <tr>
                <?php foreach ($additionals as $value) { 
                    if($value->additional->type_additional == 'Фрезеровка') {
                ?>
                    <th style="text-align:center;"><?=$value->additional->name?></th>
                <?php } } ?>
            </tr>
            <tr>
                <?php foreach ($additionals as $value) { 
                    if($value->additional->type_additional == 'Фрезеровка') {
                ?>
                    <td><?=$value->count?></td>
                <?php } } ?>
            </tr>
        </tbody>
    </table>

    <br>
    <br>
    <table style="width:100%; border: 0; border-collapse: collapse; text-align: left;" border="1" >
        <tr >
            <td colspan="3" style=" text-align: left; font-size: 20px; padding-left: 10px;">Присадка (общее количество)</td>
        </tr>
        <tbody>
            <tr>
                <?php foreach ($additionals as $value) { 
                    if($value->additional->type_additional == 'Присадка') {
                ?>
                    <th style="text-align:center;"><?=$value->additional->name?></th>
                <?php } } ?>
            </tr>
            <tr>
                <?php foreach ($additionals as $value) { 
                    if($value->additional->type_additional == 'Присадка') {
                ?>
                    <td><?=$value->count?></td>
                <?php } } ?>
            </tr>
        </tbody>
    </table>
    <p>Итого: <b><?= $model->price?> руб</b></p>
    <h4>Комментарий:</h4>
    <p><?= $model->comment?></p>
    <br><br>
    <p>------- Конец пересылаемого сообщения --------</p>
</div>